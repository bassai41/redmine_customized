#!/bin/bash

source .env

folder=backup_$(date +%y%m%d-%H%M%S)
mkdir -p backup/$folder
docker-compose exec -it db mysqldump -u $db_user -p$db_password redmine > ./backup/$folder/dump.dmp
docker-compose exec redmine tar zcvf /usr/src/redmine/files/files.tar.gz /usr/src/redmine/files 
docker-compose cp redmine:/usr/src/redmine/files/files.tar.gz ./backup/$folder/files.tar.gz
docker-compose exec -it redmine rm /usr/src/redmine/files/files.tar.gz