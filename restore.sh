#!/bin/bash
source .env

# MySQL dump file
docker-compose exec -T db mysql -u $db_user -p$db_password redmine < ./init/dump.dmp

# redmine attached files
docker-compose cp ./init/files.tar.gz redmine:/usr/src/redmine/files/files.tar.gz
docker-compose exec -T redmine tar xvf /usr/src/redmine/files/files.tar.gz
docker-compose exec -T redmine rm /usr/src/redmine/files/files.tar.gz